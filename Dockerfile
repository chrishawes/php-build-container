FROM php:7-cli
MAINTAINER chrishawes

ENV COMPOSER_VERSION 1.0.0-alpha11

RUN apt-get update && apt-get install -y \
        git \
        libzip-dev \
    && docker-php-ext-install zip

COPY php.ini /usr/local/etc/php/conf.d/php.ini

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer --version=${COMPOSER_VERSION}

ENV COMPOSER_HOME "/composer"
ENV COMPOSER_VENDOR_DIR "/composer/vendor"
ENV COMPOSER_BIN_DIR "/composer/bin"

RUN composer global require phpunit/phpunit phing/phing phpmd/phpmd squizlabs/php_codesniffer

ENV PATH "/composer/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
